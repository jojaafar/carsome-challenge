
## Carsome Challenge 

Imagine you have been given the task of revamping the cloud infrastructure and automation tools of Carsome’s bidding platform. After multiple interviews with the stakeholders within Carsome, you came to the following conclusions:

1. Carsome bidding platform is operated based on an open bidding format where all dealers login during the bidding session to bid for their desired used cars.
2. All bidding information (via real-time APIs) such as the number of bids, number of bidders, number of viewers and the latest bidding price are made known to everyone.
3. Any successful bid, during the last 30 seconds of the bidding session, will reset the timer of that specific car to back to 30 seconds (thus extending the timer for that specific car).
4. To maximise profit, most dealers will tend to snipe bid during the last minute of the bidding session.
5. When the timer stopped, dealer with the highest bid (for each of the individual used cars) will win the right to purchase that specific used car.
6. Due to legacy problems, the codes for the bidding platform weren’t well maintained and written which causes slowness/failures when the bidding session ends.
7. As the number of used cars and concurrent login-ed dealers grew, the existing architecture fails to scale together with the business Carsome Bidding platform - Desktop web view (left) and mobile web view (right).

```
[logo]: https://gitlab.com/jojaafar/carsome-challenge/-/blob/master/img/carsome-1.png
```
[Carsome Desktop View](https://gitlab.com/jojaafar/carsome-challenge/-/blob/master/img/carsome-1.png)

```
[logo]: https://gitlab.com/jojaafar/carsome-challenge/-/blob/master/img/carsome-1.png
```
[Carsome Mobile View](https://gitlab.com/jojaafar/carsome-challenge/-/blob/master/img/carsome-2.png)

**Requirement**
Given that this open bidding is here to stay, and business will definitely outgrow the existing bidding platform. Make any assumptions if needed and propose how you would implement CI/CD pipeline and automated testing for the above scenario:

1. Assume that there’s no CI/CD pipeline and any form of automated testing.
2. Tech team is required to be able to deploy enhancements/fixes on a daily basis.
3. You could either express you solution via any form of diagrams, flowcharts and/ or write-outs.
4. Bonus if you could come out with a working.

Answer: 

 1. Refer below cronjob method
 2. Refer to question no3. 
 3. Refer to question no2, the attachment of flow process and diagram.
 4. Kindly refer to folder of automated file.  Look for YAML file or Python File.




