## This is sample psuedo code on xml runner for jenkins to test the integration
## Essentially any CI solution will support this format, and of course include jenkins

if __name__ == '__main__':
    ############# Add these lines #############
    import xmlrunner
    runner = xmlrunner.XMLTestRunner(output='test-reports')
    unittest.main(testRunner=runner)
    ###########################################
    unittest.main()

